#!/bin/bash 
# Specific scripts/rpmbuild_.sh for frontier-squid
# To be run before running the generic scripts/rpmbuild_.sh 
# Run this from this directory, scripts,

source ../../scripts/utilities.sh

specDefine2var release4source
specDefine2var shoalversion

cd ../SOURCES

echo "Version: ${Version}"
echo "release4source: ${release4source}"
echo "shoalversion: ${shoalversion}"
export Version release4source shoalversion

set -ex
wget --no-verbose "http://frontier.cern.ch/dist/frontier-squid-${Version}-${release4source}.tar.gz"
wget --no-verbose "https://github.com/hep-gc/shoal/archive/shoal-agent-${shoalversion}.tar.gz"
BASE=shoal-downloads
export BASE
rm -f .pipdone
(
set -ex
mkdir -p $BASE/.local
# newer pip3 needed for cryptography
pip3 install -U --no-cache-dir --prefix $PWD/$BASE/.local pip
PATH=$PWD/$BASE/.local/bin:$PATH
export PYTHONPATH=`echo $PWD/$BASE/.local/lib/*/site-packages`
PIPOPTS="download --no-cache-dir -d $PWD/$BASE"
pip3 $PIPOPTS pyinstaller
pip3 $PIPOPTS wheel
pip3 $PIPOPTS pyOpenSSL
pip3 $PIPOPTS netifaces
pip3 $PIPOPTS pika
pip3 $PIPOPTS pystun3
pip3 $PIPOPTS requests
touch .pipdone
# piping the output removes the ugly download progress messages
# but it hides error codes, so that's what .pipdone is for
)|cat
test -f .pipdone
tar czvf $BASE.tar.gz $BASE
rm -rf .pipdone $BASE
